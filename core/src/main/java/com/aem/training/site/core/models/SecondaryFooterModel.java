package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Social;
import com.aem.training.site.core.beans.UseFullLink;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SecondaryFooterModel {

    private final static Logger logger = LoggerFactory.getLogger(SecondaryFooterModel.class);

    @ValueMapValue
    @Default(values = "Ninestars\n" +
            "A108 Adam Street\n" +
            "New York, NY 535022\n" +
            "United States\n" +
            "\n" +
            "Phone: +1 5589 55488 55\n" +
            "Email: info@example.com")
    private String leftheading;

    @ValueMapValue
    @Default(values = " Our Social Networks\n" +
            "Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies")
    private String rightheading;

    @ChildResource
    Resource socialicon;
    List<Social> LisofIcon;

    @ChildResource
    Resource usefullinks;
    List<UseFullLink> Lisofusefullinks;

    @ChildResource
    Resource listofservices;
    List<UseFullLink> Lisofservices;

    @PostConstruct
    protected void init() {
        logger.debug("heading :::::::::::{}", leftheading);
        logger.debug("subHeading ::::::::::::{}", rightheading);
        LisofIcon = new ArrayList<>();
        if (socialicon != null){
            socialicon.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String link = valueMap.get("link",StringUtils.EMPTY);
                String classname = valueMap.get("classname",StringUtils.EMPTY);
                String icontext = valueMap.get("icontext", StringUtils.EMPTY);

                Social social = new Social(link,classname,icontext);
                LisofIcon.add(social);
                logger.debug("LisofIcon ::::::::::::{}", LisofIcon);
            });
        }

        Lisofusefullinks = new ArrayList<>();
        if(usefullinks != null){
            usefullinks.listChildren().forEachRemaining(items ->{
                ValueMap valueMap = items.getValueMap();
                String icon = valueMap.get("icon",StringUtils.EMPTY);
                String link = valueMap.get("link",StringUtils.EMPTY);
                String text = valueMap.get("text",StringUtils.EMPTY);
                UseFullLink useFullLink =new UseFullLink(icon,link,text);
                Lisofusefullinks.add(useFullLink);
            });
        }

        Lisofservices =new ArrayList<>();
        if(listofservices != null){
            listofservices.listChildren().forEachRemaining(item ->{
                ValueMap valueMap = item.getValueMap();
                String icon = valueMap.get("icon",StringUtils.EMPTY);
                String link = valueMap.get("link",StringUtils.EMPTY);
                String text = valueMap.get("text",StringUtils.EMPTY);
                UseFullLink useservices =new UseFullLink(icon,link,text);
                Lisofservices.add(useservices);
            });
        }

    }

    public String getLeftheading() {
        return leftheading;
    }

    public String getRightheading() {
        return rightheading;
    }

    public List<Social> getLisofIcon() {
        logger.debug("LisofIcon ::::::::::::{}", LisofIcon);
        return LisofIcon;
    }

    public List<UseFullLink> getLisofusefullinks() {
        return Lisofusefullinks;
    }

    public List<UseFullLink> getLisofservices() {
        return Lisofservices;
    }
}
