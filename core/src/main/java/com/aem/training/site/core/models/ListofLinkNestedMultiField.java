package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Info;
import com.aem.training.site.core.beans.Infom;
import com.aem.training.site.core.beans.Infomation;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListofLinkNestedMultiField {
   private final static Logger log = LoggerFactory.getLogger(ListofLinkNestedMultiField.class);
    @ChildResource
    private Resource list;

    private List<Infomation> listofinfo;

    @PostConstruct
    protected void init() {
        listofinfo = new ArrayList<>();
        if (list != null) {
            list.listChildren().forEachRemaining(item -> {
                ValueMap valueMap = item.getValueMap();
                String text = valueMap.get("text", StringUtils.EMPTY);
                String link = valueMap.get("link", StringUtils.EMPTY);
                log.debug("text ::::::::::::{}",text);
                log.debug("link ::::::::::::{}",link);
                List<Infom> infochildlist = new ArrayList<>();
                Resource infochildResource = item.getChild("childinfo");
                if (infochildResource != null) {
                    infochildResource.listChildren().forEachRemaining(itemchild -> {
                        ValueMap valueMap1 = itemchild.getValueMap();
                        String textchild = valueMap1.get("text", StringUtils.EMPTY);
                        String linkchild = valueMap1.get("link", StringUtils.EMPTY);
                        log.debug("textchild ::::::::::::{}",textchild);
                        log.debug("linkchild ::::::::::::{}",linkchild);
                        Infom infoobj = new Infom(textchild, linkchild);
                        infochildlist.add(infoobj);
                    });
                }
                Infomation infomation = new Infomation(text, link, infochildlist);
                listofinfo.add(infomation);
                log.debug("listofinfo ::::::::::::::::::: {}",listofinfo);
            });
        }

    }

    public List<Infomation> getListofinfo() {
        log.debug("listofinfo ::::::::::::::::::: {}",listofinfo);

        return listofinfo;
    }
}
