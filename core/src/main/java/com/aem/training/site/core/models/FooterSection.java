package com.aem.training.site.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterSection {

    private final static Logger logger =LoggerFactory.getLogger(FooterSection.class);
    @ValueMapValue
    @Default(values = "Ninestars")
    private String leftText;

    @ValueMapValue
    @Default(values = "BootstrapMade")
    private String rightText;

    @ValueMapValue
    private String link;

    @PostConstruct
    protected void inti(){
        logger.debug("leftText :{}",leftText);
        logger.debug("rightText :{}",rightText);
        logger.debug("link :{}",link);
    }

    public String getLeftText() {
        return leftText;
    }

    public String getRightText() {
        return rightText;
    }

    public String getLink() {
        return link;
    }
}
