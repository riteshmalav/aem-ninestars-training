package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Question;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FAQSectionModel {

    private final static Logger logger = LoggerFactory.getLogger(ClientSectionModel.class);

    @ValueMapValue
    @Default(values = "F.A.Q")
    private String heading;

    @ValueMapValue
    @Default(values = "Frequently Asked Questions")
    private String subHeading;

    @ChildResource
    Resource question;
    List<Question> Questionlist;

    @PostConstruct
    protected void init() {
        logger.debug("heading :{}", heading);
        logger.debug("subHeading :{}", subHeading);

        Questionlist = new ArrayList<>();
        if (question != null) {
            question.listChildren().forEachRemaining(item -> {
                ValueMap valueMap = item.getValueMap();
                String questionfaq = valueMap.get("question", StringUtils.EMPTY);
                String answer = valueMap.get("answer", StringUtils.EMPTY);
                Question qty = new Question(questionfaq, answer);
                Questionlist.add(qty);
                logger.debug("Questionlist :{}", Questionlist);
            });
        }
    }

    public String getHeading() {
        logger.debug("heading :{}", heading);
        return heading;
    }

    public String getSubHeading() {
        logger.debug("subHeading :{}", subHeading);
        return subHeading;
    }

    public List<com.aem.training.site.core.beans.Question> getQuestionlist() {
        logger.debug("Questionlist :{}", Questionlist);
        return Questionlist;
    }
}
