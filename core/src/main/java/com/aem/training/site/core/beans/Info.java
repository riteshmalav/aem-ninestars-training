package com.aem.training.site.core.beans;

public class Info {

    private String text;
    private String link;
    private String image;

    public Info(String text, String link, String image) {
        this.text = text;
        this.link = link;
        this.image = image;
    }

    public Info(String text, String link) {
        this.text = text;
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }

    public String getImage() {
        return image;
    }
}
