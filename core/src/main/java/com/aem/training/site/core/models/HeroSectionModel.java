package com.aem.training.site.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeroSectionModel {

    private final static Logger logger = LoggerFactory.getLogger(HeroSectionModel.class);
    @ValueMapValue
    @Default(values = "Bettter digital experience with Ninestars")
    private String heading;
    @ValueMapValue
    @Default(values = "We are team of talented designers making websites with Bootstrap")
    private String discription;
    @ValueMapValue
    private String ctaLink;
    @ValueMapValue
    private String image;

    @PostConstruct
    protected void init() {
        logger.debug("heading ::{}", heading);
        logger.debug("discription ::{}", discription);
        logger.debug("ctaLink ::{}", ctaLink);
        logger.debug("image ::{}", image);
    }

    public String getHeading() {
        return heading;
    }

    public String getDiscription() {
        return discription;
    }

    public String getCtaLink() {
        return ctaLink;
    }

    public String getImage() {
        return image;
    }
}
