package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.ImageCategory;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;


@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GallerySection {

    private final static Logger logger = LoggerFactory.getLogger(AboutSectionModel.class);
    @ValueMapValue
    @Default(values = "Portfolio")
    private String heading;

    @ValueMapValue
    @Default(values = "Check out our beautifull portfolio")
    private String Subheading;

    @ChildResource
    Resource filterImage;

    private List<ImageCategory> imageList;

    @PostConstruct
    protected void init() {

        logger.debug("heading ::{}", heading);
        logger.debug("Subheading ::{}", Subheading);

        imageList = new ArrayList<>();
        if (filterImage != null) {
            filterImage.listChildren().forEachRemaining(item -> {
                ValueMap valueMap = item.getValueMap();
                String category = valueMap.get("category", StringUtils.EMPTY);
                String Image = valueMap.get("image", StringUtils.EMPTY);
                ImageCategory imageCategory = new ImageCategory(category, Image);
                imageList.add(imageCategory);
                logger.debug("ImageList ::{}", imageList);
            });
        }
        logger.debug("ImageList ::{}", imageList);
    }

    public String getHeading() {
        return heading;
    }

    public String getSubheading() {
        return Subheading;
    }

    public List<ImageCategory> getImageList() {
        logger.debug("ImageList ::{}", imageList);
        return imageList;
    }
}
