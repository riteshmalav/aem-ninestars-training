package com.aem.training.site.core.beans;

import java.util.List;

public class NavList {
    private String text;

    private String link;

    private List<Nav>listoflink;

    public NavList(String text, String link, List<Nav> listoflink) {
        this.text = text;
        this.link = link;
        this.listoflink = listoflink;
    }



    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }

    public List<Nav> getListoflink() {
        return listoflink;
    }
}
