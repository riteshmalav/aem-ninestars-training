package com.aem.training.site.core.beans;

public class User {

    private String address;
    private String email;
    private String phone;

    public User(String address, String email, String phone) {
        this.address = address;
        this.email = email;
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
