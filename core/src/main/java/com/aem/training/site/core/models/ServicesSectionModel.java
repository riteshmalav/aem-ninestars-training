package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Cards;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ServicesSectionModel {
    private final static Logger logger = LoggerFactory.getLogger(ClientSectionModel.class);
    @ValueMapValue
    @Default(values = "Services")
    private String heading;

    @ValueMapValue
    @Default(values = "Check out the great services we offer")
    private String subHeading;

    @ChildResource
    Resource card;

    List<Cards> ListOfCard;

    @PostConstruct
    protected void init() {
        logger.debug("heading :{}", heading);
        logger.debug("subHeading :{}", subHeading);

        ListOfCard =new ArrayList<>();
        if (card != null){
            card.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String icon = valueMap.get(     "icon",StringUtils.EMPTY);
                String cardheading = valueMap.get("cardheading",StringUtils.EMPTY);
                String carddiscription = valueMap.get("carddiscription",StringUtils.EMPTY);
                Cards cards =new Cards(icon,cardheading,carddiscription);
                ListOfCard.add(cards);
            });
    }

}

    public String getHeading() {
        return heading;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public List<Cards> getListOfCard() {
        return ListOfCard;
    }
}
