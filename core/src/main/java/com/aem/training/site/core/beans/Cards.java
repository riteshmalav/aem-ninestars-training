package com.aem.training.site.core.beans;

public class Cards {
    private String icon;
    private String cardheading;
    private String carddiscription;


    public Cards(String icon, String cardheading, String carddiscription) {
        this.icon = icon;
        this.cardheading = cardheading;
        this.carddiscription = carddiscription;
    }

    public String getSubheading() {
        return cardheading;
    }

    public String getSubdiscription() {
        return carddiscription;
    }

    public String getIcon() {
        return icon;
    }
}
