package com.aem.training.site.core.beans;

import java.util.List;

public class Infomation {

    private String text;
    private String link;
    private List<Infom> listinfo;

    public Infomation(String text, String link, List<Infom> listinfo) {
        this.text = text;
        this.link = link;
        this.listinfo = listinfo;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }

    public List<Infom> getListinfo() {
        return listinfo;
    }
}
