package com.aem.training.site.core.beans;

public class AboutBean {

    private String subheading;

    private String subdiscription;

    private String icon;

    public AboutBean(String subheading, String subdiscription, String icon) {
        this.subheading = subheading;
        this.subdiscription = subdiscription;
        this.icon = icon;
    }

    public String getSubheading() {
        return subheading;
    }

    public String getSubdiscription() {
        return subdiscription;
    }

    public String getIcon() {
        return icon;
    }
}
