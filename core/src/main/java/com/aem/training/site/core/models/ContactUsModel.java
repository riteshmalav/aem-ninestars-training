package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.User;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContactUsModel {

     private final static Logger logger = LoggerFactory.getLogger(ContactUsModel.class);
    @ValueMapValue
    @Default(values = "Contact Us")
    private String heading;

    @ValueMapValue
    @Default(values = "Contact us the get started")
    private String subheading;

    @ChildResource
    Resource userlocation;

    List<User> userdata;

    @PostConstruct
    protected void init(){
        logger.debug("heading :{}",heading);
        logger.debug("subheading :{}",subheading);
        userdata = new ArrayList<>();
        if (userlocation !=null){
            userlocation.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String address =valueMap.get("address", StringUtils.EMPTY);
                String email =valueMap.get("email", StringUtils.EMPTY);
                String phone =valueMap.get("phone", StringUtils.EMPTY);

                User user = new User(address,email,phone);
                userdata.add(user);
            });
        }
    }

    public String getHeading() {
        return heading;
    }

    public String getSubheading() {
        return subheading;
    }

    public List<User> getUserdata() {
        return userdata;
    }
}
