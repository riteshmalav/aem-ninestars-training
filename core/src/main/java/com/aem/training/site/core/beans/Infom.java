package com.aem.training.site.core.beans;

public class Infom {
    private String text;
    private String link;

    public Infom(String text, String link) {
        this.text = text;
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }
}
