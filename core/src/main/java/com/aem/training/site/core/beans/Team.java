package com.aem.training.site.core.beans;

public class Team {

    private String name;
    private String designation;
    private String image;

    public Team(String name, String designation, String image) {
        this.name = name;
        this.designation = designation;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public String getImage() {
        return image;
    }
}
