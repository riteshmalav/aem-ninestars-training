package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.AboutBean;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AboutSectionModel {
    private final static Logger logger = LoggerFactory.getLogger(AboutSectionModel.class);

    @ValueMapValue
    private String image;

    @ValueMapValue
    @Default(values = "Voluptatem dignissimos provident quasi")
    private String heading;

    @ValueMapValue
    @Default(values = "  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit")
    private String discription;

    @ChildResource
    private Resource about;

    private List<AboutBean> AboutList;

    @PostConstruct
    protected void init() {
        logger.debug("image ::{}", image);
        logger.debug("heading ::{}", heading);
        logger.debug("discription ::{}", discription);
        AboutList = new ArrayList<>();
        if (about != null) {
            about.listChildren().forEachRemaining(item -> {
                ValueMap valueMap = item.getValueMap();
                String subheading = valueMap.get("subheading", StringUtils.EMPTY);
                String subdiscription = valueMap.get("subdiscription", StringUtils.EMPTY);
                String icon = valueMap.get("icon", StringUtils.EMPTY);
                AboutBean aboutBean = new AboutBean(subheading, subdiscription, icon);
                AboutList.add(aboutBean);
            });
        }
    }

    public String getImage() {
        return image;
    }

    public String getHeading() {
        return heading;
    }

    public String getDiscription() {
        return discription;
    }

    public List<AboutBean> getAboutList() {
        return AboutList;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public void setAboutList(List<AboutBean> aboutList) {
        AboutList = aboutList;
    }
}
