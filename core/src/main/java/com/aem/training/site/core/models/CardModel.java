package com.aem.training.site.core.models;

import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.apache.sling.api.resource.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CardModel {
    private static Logger logger = LoggerFactory.getLogger(CardModel.class);
    @ValueMapValue
    private String cardimage;
    @ValueMapValue
    @Default(values = "Ritesh MALAV")
    private String fullname;
    @ValueMapValue
    @Default(values = "enter YOUR discription")
    private String discription;
    @ValueMapValue
    @Default(values = "Avatar")
    private String altText;

    @PostConstruct
    protected void init() {
        logger.info("hello CARD model ");
        logger.info("Card image :{}", cardimage);
        logger.info("Card fullname :{}", fullname);
        logger.info("Card discription :{}", discription);
        logger.info("Card alt Text :{}", altText);
    }

    public String getCardimage() {
        return cardimage;
    }

    public String getFullname() {
        return fullname;
    }

    public String getDiscription() {
        return discription;
    }

    public String getAltText() {
        return altText;
    }
}
