package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Nav;
import com.aem.training.site.core.beans.NavList;
import com.aem.training.site.core.beans.navlistoflist;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderModel {

    private final static Logger logger = LoggerFactory.getLogger(HeaderModel.class);

    @ValueMapValue
    private String logo;

    @ValueMapValue
    private String logolink;

    @ChildResource
    Resource link;

    List<navlistoflist> listofNav;

    @PostConstruct
    protected void init() {
    logger.debug("logo :{}",logo);
    logger.debug("logolink :{}",logolink);

        listofNav =new ArrayList<>();
        if(link != null){
            link.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String text = valueMap.get("text", StringUtils.EMPTY);
                String link = valueMap.get("link",StringUtils.EMPTY);
                logger.debug("text :{}",text);
                logger.debug("link :{}",link);

                List<NavList>NavList = new ArrayList<>();
                Resource navResource =item.getChild("navlinks");

                if (navResource != null){
                    navResource.listChildren().forEachRemaining(navitem->{
                        ValueMap valmap1 = navitem.getValueMap();
                        String navtext = valmap1.get("text", StringUtils.EMPTY);
                        String navlink = valmap1.get("link",StringUtils.EMPTY);
                        logger.debug("navitem text ::{}",navtext);
                        logger.debug("navitem link : :{}",navlink);

                        List<Nav>subNavList = new ArrayList<>();
                        Resource subnavResource =navitem.getChild("subnavlinks");

                        if (subnavResource != null ){
                            subnavResource.listChildren().forEachRemaining(subnavitem->{
                                ValueMap valmap2 = subnavitem.getValueMap();
                                String subnavtext = valmap2.get("text", StringUtils.EMPTY);
                                String subnavlink = valmap2.get("link",StringUtils.EMPTY);
                                logger.debug("childnavtext ::::{}",subnavtext);
                                logger.debug("childnavlink ::::{}",subnavlink);

                                Nav navth =new Nav(subnavtext,subnavlink);
                                subNavList.add(navth);
                            });
                        }
                        NavList navs =new NavList(navtext,navlink,subNavList);
                        NavList.add(navs);
                    });
                }
                navlistoflist navf =new navlistoflist(text,link,NavList);
                listofNav.add(navf);
            });
        }
    }

    public String getLogo() {
        return logo;
    }

    public String getLogolink() {
        return logolink;
    }

    public List<navlistoflist> getListofNav() {
        logger.debug("listofNav :{}",listofNav);
        return listofNav;
    }
}
