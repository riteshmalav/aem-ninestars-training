package com.aem.training.site.core.beans;

public class ImageCategory {

    private String Category;
    private String Image;

    public ImageCategory(String category, String image) {
        Category = category;
        Image = image;
    }

    public String getCategory() {
        return Category;
    }

    public String getImage() {
        return Image;
    }
}
