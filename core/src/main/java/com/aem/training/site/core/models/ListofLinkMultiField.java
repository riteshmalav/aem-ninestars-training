package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Info;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.api.resource.Resource;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListofLinkMultiField {

    @ChildResource
    private Resource info;

    List <Info> infoList;

    @PostConstruct
    protected void init(){
        infoList =new ArrayList<>();
        if(info != null){
            info.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String text = valueMap.get("text", StringUtils.EMPTY);
                String link = valueMap.get("link", StringUtils.EMPTY);
                String image = valueMap.get("image", StringUtils.EMPTY);
                Info infoobj =new Info(text,link,image);
                infoList.add(infoobj);
            });
        }
    }

    public List<Info> getInfoList() {
        return infoList;
    }
}
