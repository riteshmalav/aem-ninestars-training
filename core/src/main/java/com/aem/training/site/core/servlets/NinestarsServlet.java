package com.aem.training.site.core.servlets;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.ServletResolverConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.service.component.annotations.Component;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

@Component(service = {Servlet.class}, property = {
        ServletResolverConstants.SLING_SERVLET_METHODS + "=" + HttpConstants.METHOD_POST,
        ServletResolverConstants.SLING_SERVLET_PATHS + "=" + "/bin/Ninestarsregistrationform"
})
public class NinestarsServlet extends SlingAllMethodsServlet {

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        String email = request.getParameter("email");
        String fullName = request.getParameter("fullname");
        String subject = request.getParameter("subject");
        String message = request.getParameter("message");

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("Email", email);
        jsonObject.addProperty("Full Name ", fullName);
        jsonObject.addProperty("subject", subject);
        jsonObject.addProperty("message", message);

        Gson gson = new Gson();
        out.println(gson.toJson(jsonObject));
        response.flushBuffer();
    }
}
