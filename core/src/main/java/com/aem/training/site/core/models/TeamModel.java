package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.Team;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TeamModel {
    private final static Logger logger = LoggerFactory.getLogger(ClientSectionModel.class);
    @ValueMapValue
    @Default(values = "Team")
    private String Teamheading;

    @ValueMapValue
    @Default(values = "Our team is always here to help")
    private String TeamsubHeading;

    @ChildResource
    Resource team;

    List<Team> Teamcardlist;

    @PostConstruct
    protected void init() {
        logger.debug("heading :{}", Teamheading);
        logger.debug("subHeading :{}", TeamsubHeading);

        Teamcardlist = new ArrayList<>();
        if (team != null) {
            team.listChildren().forEachRemaining(item -> {
                ValueMap valueMap = item.getValueMap();
                String name = valueMap.get("name", StringUtils.EMPTY);
                String designation = valueMap.get("designation", StringUtils.EMPTY);
                String image = valueMap.get("image", StringUtils.EMPTY);
                Team team = new Team(name, designation, image);
                Teamcardlist.add(team);
            });
        }
    }

    public String getHeading() {
        return Teamheading;
    }

    public String getSubHeading() {
        return TeamsubHeading;
    }

    public List<Team> getTeamcardlist() {
        return Teamcardlist;
    }
}

