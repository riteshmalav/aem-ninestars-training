package com.aem.training.site.core.models;

import com.aem.training.site.core.beans.ProductInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PortfolioDetails {

    private final static Logger logger= LoggerFactory.getLogger(PortfolioDetails.class);
    @ValueMapValue
    @Default(values = "Project information")
    private String heading;

    @ValueMapValue
    @Default(values = "This is an example of portfolio detail")
    private String subheading;

    @ValueMapValue
    @Default(values = "Autem ipsum nam porro corporis rerum. Quis eos dolorem eos itaque inventore commodi labore quia quia. Exercitationem repudiandae officiis neque suscipit non officia eaque itaque enim. Voluptatem officia accusantium nesciunt est omnis tempora consectetur dignissimos. Sequi nulla at esse enim cum deserunt eius.")
    private String discription;

    @ValueMapValue
    @Default( values = "/content/dam/training/ninestar/portfolio-2.jpg")
    private String image;

    @ChildResource
    Resource product;

    @ValueMapValue
    List<ProductInfo>productInfoList;



    @PostConstruct
    protected  void init(){
        logger.debug("image",image);

        productInfoList = new ArrayList<>();
        if (product != null){
            product.listChildren().forEachRemaining(item->{
                ValueMap valueMap = item.getValueMap();
                String filedName = valueMap.get("filedName", StringUtils.EMPTY);
                String fieldValue = valueMap.get("fieldValue",StringUtils.EMPTY);

                ProductInfo productInfo = new ProductInfo(filedName,fieldValue);
                productInfoList.add(productInfo);
                logger.debug("productInfo ::{}",productInfoList);
            });
        }
        logger.debug("productInfo ::{}",productInfoList);
        logger.debug("heading ::{}",heading);
        logger.debug("discription ::{}",discription);
    }

    public String getImage() {
        return image;
    }

    public List<ProductInfo> getProductInfoList() {
        return productInfoList;
    }

    public String getHeading() {
        return heading;
    }

    public String getSubheading() {
        return subheading;
    }

    public String getDiscription() {
        return discription;
    }
}
