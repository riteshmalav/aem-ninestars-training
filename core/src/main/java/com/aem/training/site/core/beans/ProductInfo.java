package com.aem.training.site.core.beans;

public class ProductInfo {

    private String filedName;
    private String fieldValue;

    public ProductInfo(String filedName, String fieldValue) {
        this.filedName = filedName;
        this.fieldValue = fieldValue;
    }

    public String getFiledName() {
        return filedName;
    }

    public String getFieldValue() {
        return fieldValue;
    }
}
