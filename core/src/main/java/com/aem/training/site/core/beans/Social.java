package com.aem.training.site.core.beans;

public class Social {

    private String Link;
    private String classname;
    private String icontext;

    public Social(String link, String classname, String icontext) {
        Link = link;
        this.classname = classname;
        this.icontext = icontext;
    }

    public String getLink() {
        return Link;
    }

    public String getClassname() {
        return classname;
    }

    public String getIcontext() {
        return icontext;
    }
}
