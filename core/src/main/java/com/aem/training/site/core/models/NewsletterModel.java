package com.aem.training.site.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NewsletterModel {
    @ValueMapValue
    @Default(values = "Join Our Newsletter")
    private String title;

    @ValueMapValue
    @Default(values = "Tamen quem nulla quae legam multos aute sint culpa legam noster magna")
    private String discription;
    private final static Logger logger = LoggerFactory.getLogger(NewsletterModel.class);

    @PostConstruct
    protected void init() {
        logger.debug("title : {}", title);
        logger.debug("discription :{}", discription);
    }

    public String getTitle() {
        return title;
    }

    public String getDiscription() {
        return discription;
    }
}
