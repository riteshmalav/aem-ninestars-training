package com.aem.training.site.core.models;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ClientSectionModel {

    private final static Logger logger = LoggerFactory.getLogger(ClientSectionModel.class);
    @ValueMapValue
    @Default(values = "Clients")
    private String heading;

    @ValueMapValue
    @Default(values = "They trusted us")
    private String subHeading;

    @ChildResource
    Resource image;

    List<String> imagelist;

    @PostConstruct
    protected void init() {
        logger.debug("heading :{}", heading);
        logger.debug("subHeading :{}", subHeading);

        imagelist = new ArrayList<>();
        if (image != null) {
            image.listChildren().forEachRemaining(item -> {
                ValueMap valueMap = item.getValueMap();
                String imagec = valueMap.get("clientimage", StringUtils.EMPTY);
                imagelist.add(imagec);
            });
        }
    }

    public String getHeading() {
        return heading;
    }

    public String getSubHeading() {
        return subHeading;
    }

    public List<String> getImagelist() {
        return imagelist;
    }
}
