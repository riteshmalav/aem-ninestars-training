package com.aem.training.site.core.beans;

import java.util.List;

public class navlistoflist {
    private String text;

    private String link;
    private List<NavList> listoflink;

    public navlistoflist(String text, String link, List<NavList> listoflink) {
        this.text = text;
        this.link = link;
        this.listoflink = listoflink;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }

    public List<NavList> getListoflink() {
        return listoflink;
    }
}
