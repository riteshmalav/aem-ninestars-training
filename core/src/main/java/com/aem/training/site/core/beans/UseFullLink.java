package com.aem.training.site.core.beans;

public class UseFullLink {
    private String icon;

    private String Link;

    private String Text;

    public UseFullLink(String icon, String link, String text) {
        this.icon = icon;
        Link = link;
        Text = text;
    }

    public String getIcon() {
        return icon;
    }

    public String getLink() {
        return Link;
    }

    public String getText() {
        return Text;
    }
}
