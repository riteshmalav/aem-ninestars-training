package com.aem.training.site.core.beans;

public class Nav {

    private String text;

    private String link;

    public Nav(String text, String link) {
        this.text = text;
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }
}
